from django.contrib import admin
from django.urls import path, include

from rooms.views import get_room, create_room

urlpatterns = [
    path('<str:room_code>/', get_room, name='get-room'),
    path('add', create_room, name='create-room'),
]
