from channels.consumer import SyncConsumer
from channels.generic.websocket import WebsocketConsumer
from django.http import Http404, JsonResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from rooms.models import Room
from rooms.utils import room_utils


def get_room(request, room_code):
    try:
        room = Room.objects.get(code=room_code)
        return JsonResponse(data={'id': room.id}, status=200)
    except Room.DoesNotExist:
        return JsonResponse(data={'message': 'No Room matches the given query.'}, status=400)


@csrf_exempt
def create_room(request):
    random_code = room_utils.get_random_code()
    room = Room.objects.create(code=random_code)
    return JsonResponse(data={
        'id': room.id,
        'code': room.code
    }, status=201)
