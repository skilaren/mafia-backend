from math import ceil

from django.db import models
from django.db.models import Sum


class Room(models.Model):
    class State(models.TextChoices):
        JOINING = 'JOINING', 'Players are joining'
        GAME_START = 'GAME_START', 'Roles assigned, the game begins...'
        NIGHT = 'NIGHT', 'The night has come...'
        DAY = 'DAY', 'A new day begins'
        GAME_END = 'GAME_END', 'The game ended!'

    id = models.BigAutoField(primary_key=True, verbose_name='ID')
    code = models.CharField(max_length=10, verbose_name='Room code', default='')
    state = models.CharField(max_length=255, choices=State.choices, default=State.JOINING)
    nights_count = models.PositiveIntegerField(default=0)
    days_count = models.PositiveIntegerField(default=0)


class Player(models.Model):
    class Role(models.TextChoices):
        MAFIA = 'MAFIA', 'Mafia'
        CITIZEN = 'CITIZEN', 'Citizen'

    name = models.CharField(max_length=255, primary_key=True)
    role = models.CharField(max_length=255, choices=Role.choices, blank=True, null=True)
    alive = models.BooleanField(default=True)
    channel_name = models.CharField(max_length=1000, blank=True, null=True)
    night_votes = models.PositiveIntegerField(default=0)
    day_votes = models.PositiveIntegerField(default=0)

    @staticmethod
    def mafia_players_alive_count():
        return Player.objects.filter(role=Player.Role.MAFIA, alive=True).count()

    @staticmethod
    def citizen_players_alive_count():
        return Player.objects.filter(role=Player.Role.CITIZEN, alive=True).count()

    @staticmethod
    def players_alive_count():
        return Player.objects.filter(alive=True).count()

    @staticmethod
    def player_to_kill():
        return Player.objects.filter(night_votes__gte=ceil(Player.mafia_players_alive_count() / 2)).first()

    @staticmethod
    def player_to_kick():
        return Player.objects.filter(day_votes__gte=ceil(Player.players_alive_count() / 2)).first()

    @staticmethod
    def reset_night_votes():
        Player.objects.all().update(night_votes=0)

    @staticmethod
    def reset_day_votes():
        Player.objects.all().update(day_votes=0)
