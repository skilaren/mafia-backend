from django.urls import re_path

from rooms import consumers

websocket_urlpatterns = [
    re_path(r'ws/game/$', consumers.MafiaGameConsumer.as_asgi()),
]
