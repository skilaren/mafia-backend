import json
import random

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from rooms.models import Room, Player


class MafiaGameConsumer(WebsocketConsumer):

    def connect(self):
        self.accept()

    def receive(self, text_data=None, bytes_data=None):
        game_room, _ = Room.objects.get_or_create(id=1)
        text_data_dict = json.loads(text_data)
        if game_room.state == Room.State.JOINING:
            username = text_data_dict['name']
            if Player.objects.filter(name=username).exists():
                self.send(text_data=json.dumps({
                    'state': Room.State.JOINING,
                    'status': 'ALREADY_REGISTERED',
                    'message': 'This name is already taken'
                }))
            else:
                async_to_sync(self.channel_layer.group_add)('all', self.channel_name)
                Player.objects.create(name=username, channel_name=self.channel_name)
                print(username, self.channel_name)
                self.send(text_data=json.dumps({
                    'state': Room.State.JOINING,
                    'status': 'REGISTERED',
                    'message': f'You are in the game as "{username}". Waiting for other players'
                }))
                if Player.objects.all().count() == 7:
                    players = Player.objects.all()
                    sample = set(random.sample(list(players.values_list('name', flat=True)), 2))
                    for player in players:
                        if player.name in sample:
                            role = Player.Role.MAFIA
                        else:
                            role = Player.Role.CITIZEN
                        async_to_sync(self.channel_layer.group_add)(role.lower(), player.channel_name)
                        player.role = role
                        player.save()
                    async_to_sync(self.channel_layer.group_send)(Player.Role.MAFIA.lower(), {
                        'type': 'roles.send',
                        'role': Player.Role.MAFIA,
                        'mafia_players': list(Player.objects.filter(role=Player.Role.MAFIA)
                                              .values_list('name', flat=True)),
                        'citizen_players': list(Player.objects.filter(role=Player.Role.CITIZEN)
                                                .values_list('name', flat=True)),
                        'all_players': list(Player.objects.all().values_list('name', flat=True)),
                    })
                    async_to_sync(self.channel_layer.group_send)(Player.Role.CITIZEN.lower(), {
                        'type': 'roles.send',
                        'role': Player.Role.CITIZEN,
                        'all_players': list(Player.objects.all().values_list('name', flat=True)),
                    })
                    game_room.state = Room.State.NIGHT
                    game_room.nights_count += 1
                    game_room.save()
        elif game_room.state == Room.State.NIGHT:
            username = text_data_dict['name']
            if Player.objects.filter(name=username, role=Player.Role.MAFIA, alive=True).exists():
                propose_to_kill = text_data_dict.get('wantToKill')
                going_to_kill = text_data_dict.get('goingToKill')
                if propose_to_kill is not None:
                    if Player.objects.filter(name=propose_to_kill).exists():
                        async_to_sync(self.channel_layer.group_send)(Player.Role.MAFIA.lower(), {
                            'type': 'game.vote',
                            'status': 'PROPOSE_KILL',
                            'player': username,
                            'proposed_player': propose_to_kill
                        })
                    else:
                        self.send(
                            json.dumps({
                                'state': Room.State.NIGHT,
                                'status': 'PLAYER_NOT_EXIST',
                                'message': 'You want to kill somebody who is not playing this game. '
                                           'You sure you playing right game?'
                            }))
                elif going_to_kill is not None:
                    proposed_player = Player.objects.filter(name=going_to_kill)
                    if proposed_player.exists():
                        async_to_sync(self.channel_layer.group_send)(Player.Role.MAFIA.lower(), {
                            'type': 'game.vote',
                            'status': 'VOTE_KILL',
                            'player': username,
                            'proposed_player': going_to_kill
                        })
                        proposed_player = proposed_player.first()
                        proposed_player.night_votes += 1
                        proposed_player.save()
                    killed_player = Player.player_to_kill()
                    if killed_player:
                        async_to_sync(self.channel_layer.group_send)('all', {
                            'type': 'day.starts',
                            'status': 'PLAYER_KILLED',
                            'killed_player': killed_player.name
                        })
                        killed_player.alive = False
                        killed_player.save()
                        if Player.mafia_players_alive_count() == Player.citizen_players_alive_count():
                            async_to_sync(self.channel_layer.group_send)('all', {
                                'type': 'game.end',
                                'status': Room.State.GAME_END.capitalize(),
                                'winner': Player.Role.MAFIA.capitalize(),
                                'mafia_players': list(Player.objects.filter(role=Player.Role.MAFIA)
                                                      .values_list('name', flat=True)),
                            })
                            self.disconnect_all_players()
                            game_room.state = Room.State.GAME_END
                            game_room.save()
                        else:
                            Player.reset_night_votes()
                            game_room.state = Room.State.DAY
                            game_room.days_count += 1
                            game_room.save()
                    else:
                        self.send(
                            json.dumps({
                                'state': Room.State.NIGHT,
                                'status': 'PLAYER_NOT_EXIST',
                                'message': 'You want to kill somebody who is not playing this game. '
                                           'You sure you playing right game?'
                            }))
                else:
                    self.send(json.dumps({
                        'state': Room.State.NIGHT,
                        'status': 'NIGHT_NO_PLAYER_PROPOSED',
                        'message': 'You are not going to kill or will to kill somebody... '
                                   'Why have you send it?'
                    }))
            else:
                self.send(json.dumps({
                    'state': Room.State.NIGHT,
                    'status': 'NIGHT_OUT',
                    'message': 'You are out of the game at the moment, just watch.'
                }))
        elif game_room.state == Room.State.DAY:
            username = text_data_dict['name']
            if Player.objects.filter(name=username, alive=True).exists():
                propose_to_kick = text_data_dict.get('wantToKick')
                going_to_kick = text_data_dict.get('goingToKick')
                if propose_to_kick is not None:
                    if Player.objects.filter(name=propose_to_kick).exists():
                        async_to_sync(self.channel_layer.group_send)('all', {
                            'type': 'game.vote',
                            'status': 'PROPOSE_KICK',
                            'player': username,
                            'proposed_player': propose_to_kick
                        })
                    else:
                        self.send(
                            json.dumps({
                                'state': Room.State.DAY,
                                'status': 'PLAYER_NOT_EXIST',
                                'message': 'You want to kick somebody who is not playing this game. '
                                           'You sure you playing right game?'
                            }))
                elif going_to_kick is not None:
                    proposed_player = Player.objects.filter(name=going_to_kick)
                    if proposed_player.exists():
                        async_to_sync(self.channel_layer.group_send)('all', {
                            'type': 'game.vote',
                            'status': 'VOTE_KICK',
                            'player': username,
                            'proposed_player': going_to_kick
                        })
                        proposed_player = proposed_player.first()
                        proposed_player.day_votes += 1
                        proposed_player.save()
                        kicked_player = Player.player_to_kick()
                        if kicked_player:
                            async_to_sync(self.channel_layer.group_send)('all', {
                                'type': 'day.ends',
                                'status': 'PLAYER_KICKED',
                                'kicked_player': kicked_player.name
                            })
                            kicked_player.alive = False
                            kicked_player.save()
                            Player.reset_day_votes()
                            if Player.mafia_players_alive_count() == 0:
                                async_to_sync(self.channel_layer.group_send)('all', {
                                    'type': 'game.end',
                                    'winner': Player.Role.CITIZEN.capitalize(),
                                    'mafia_players': list(Player.objects.filter(role=Player.Role.MAFIA)
                                                          .values_list('name', flat=True)),
                                })
                                self.disconnect_all_players()
                                game_room.state = Room.State.GAME_END
                                game_room.save()
                            elif Player.mafia_players_alive_count() == Player.citizen_players_alive_count():
                                async_to_sync(self.channel_layer.group_send)('all', {
                                    'type': 'game.end',
                                    'winner': Player.Role.MAFIA.capitalize(),
                                    'mafia_players': list(Player.objects.filter(role=Player.Role.MAFIA)
                                                          .values_list('name', flat=True)),
                                })
                                self.disconnect_all_players()
                                game_room.state = Room.State.GAME_END
                                game_room.save()
                            else:
                                game_room.state = Room.State.NIGHT
                                game_room.nights_count += 1
                                game_room.save()
                    else:
                        self.send(
                            json.dumps({
                                'state': Room.State.DAY,
                                'status': 'PLAYER_NOT_EXIST',
                                'message': 'You want to kill somebody who is not playing this game. '
                                           'You sure you playing right game?'
                            }))
                else:
                    self.send(json.dumps({
                        'state': Room.State.DAY,
                        'status': 'DAY_NO_PLAYER_PROPOSED',
                        'message': 'You sent no players to promote to vote for... '
                                   'Are you mafia?'
                    }))
            else:
                self.send(json.dumps({
                    'state': Room.State.DAY,
                    'status': 'DAY_OUT',
                    'message': 'You are out of the game at the moment, just watch.'
                }))

    def disconnect(self, code):
        pass

    def disconnect_all_players(self):
        async_to_sync(self.channel_layer.group_send)('all', {
            'type': 'websocket.disconnect',
        })

    def roles_send(self, event):
        self.send(json.dumps({
            'state': Room.State.NIGHT,
            'role': event['role'],
            'mafiaPlayers': event.get('mafia_players') or [],
            'citizenPlayers': event.get('citizen_players') or [],
            'allPlayers': event.get('all_players') or [],
        }))

    def game_vote(self, event):
        self.send(json.dumps({
            'state': Room.State.NIGHT,
            'status': event['status'],
            'player': event['player'],
            'playerProposition': event['proposed_player'],
        }))

    def day_starts(self, event):
        self.send(json.dumps({
            'state': Room.State.DAY,
            'status': event['status'],
            'killedPlayer': event['killed_player'],
        }))

    def day_ends(self, event):
        self.send(json.dumps({
            'state': Room.State.DAY,
            'status': event['status'],
            'kickedPlayer': event['kicked_player'],
        }))

    def game_end(self, event):
        Room.objects.all().delete()
        Player.objects.all().delete()
        self.send(json.dumps({
            'state': Room.State.GAME_END,
            'winner': event['winner'],
            'mafiaPlayers': event['mafia_players'],
        }))
