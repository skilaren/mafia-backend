Запуск:

`pip install -r requirements.txt`<br/>
`python manage.py makemigrations`<br/>
`python manage.py migrate`<br/>
`python manage.py runserver [hostname:port]`

Дефолтный хост и порт

`
ws://localhost:8000/ws/game/
`

WARNING: Если что-то пошло не так - почистить базу

#### Connection

var socket2 = new WebSocket("ws://localhost:8000/ws/game/");
socket2.onopen = function() {
  console.log("Соединение установлено.");
};

socket2.onclose = function(event) {
  if (event.wasClean) {
    console.log('Соединение закрыто чисто');
  } else {
    console.log('Обрыв соединения'); // например, "убит" процесс сервера
  }
  console.log('Код: ' + event.code + ' причина: ' + event.reason);
};

socket2.onmessage = function(event) {
  console.log("Получены данные " + event.data);
};

socket2.onerror = function(error) {
  console.log("Ошибка " + error.message);
};
setTimeout( () => {socket2.send(JSON.stringify({name: "skilaren7"}))}, 2000);

# Подключение к игре

Для подключение к игре нужно отправить JSON строчку формата:

`{name: "Player1"}`

## Ответы:

### Во время ожидания всех игроков

##### OK:
`{
"state": "JOINING",
"status": "REGISTERED", 
"message": "You are in the game as \"Player1\". Waiting for other players"
}`

##### Уже есть:

`{"state": "JOINING", "status": "ALREADY_REGISTERED", "message": "This name is already taken"}`

### После того как все подключились (7 игроков)

#### Для мафии

`{"state": "NIGHT", "role": "MAFIA", "mafiaPlayers": ["skilaren4", "skilaren5"], "citizenPlayers": ["skilaren1", "skilaren2", "skilaren3", "skilaren6", "skilaren7"], "allPlayers": ["skilaren1", "skilaren2", "skilaren3", "skilaren4", "skilaren5", "skilaren6", "skilaren7"]}`

#### Для мирного

`{"state": "NIGHT", "role": "CITIZEN", "mafiaPlayers": [], "citizenPlayers": [], "allPlayers": ["skilaren1", "skilaren2", "skilaren3", "skilaren4", "skilaren5", "skilaren6", "skilaren7"]}`

# Игра

## Ночь

Игра начинается с ночи и мафия решает кого убить. 

player - кто предложил <br/> 
playerProposition - кого предложил

##### Для того чтобы предложить игрока для убийства:

`{name: "skilaren1", wantToKill: "skilaren7"}`

После этого всем мафиози присылается такое сообщение

`{"state": "NIGHT", "status": "PROPOSE_KILL", "player": "skilaren1", "playerProposition": "skilaren7"}`

##### Для выбора игрока для убийства

Игрок будет убит как только наберёт больше или 50% голосов от всей живой мафии

`{name: "skilaren1", goingToKill: "skilaren7"}`

После этого всем мафиози присылается такое сообщение

`{"state": "NIGHT", "status": "VOTE_KILL", "player": "skilaren4", "playerProposition": "skilaren1"}`

## День

##### Для того чтобы предложить игрока для отправки в тюрьму:

`{name: "skilaren1", wantToKick: "skilaren7"}`

После этого всем игрокам присылается такое сообщение

`{"state": "DAY", "status": "PROPOSE_KICK", "player": "skilaren1", "playerProposition": "skilaren7"}`

После убийства

`{"state": "DAY", "killedPlayer": "skilaren3"}`

##### Для выбора игрока для того чтобы посадить в тюрьму

Игрок будет убит посажен как только наберёт больше или 50% голосов от всех живых игроков

`{name: "skilaren1", goingToKill: "skilaren7"}`

После этого всем игрокам присылается такое сообщение

`{"state": "DAY", "status": "VOTE_KICK", "player": "skilaren4", "playerProposition": "skilaren1"}`

После того, как выбран игрок, которого посадят

`{"state": "DAY", "kickedPlayer": "skilaren3"}`


## Конец игры

Игра заканичвается после того, остаётся 0 мафии или остаётся поровну мирных и мафии

Сообщения об окончании:

`{"state": "GAME_END", "winner": "Mafia", "mafiaPlayers": ["skilaren4", "skilaren5"]}`

`{"state": "GAME_END", "winner": "Citizen", "mafiaPlayers": ["skilaren7", "skilaren4"]}`